XA.component.livefyre = (function ($, document) {

    var api = {};

    function initLivefyre(prop) {
        var articleId = fyre.conv.load.makeArticleId(null);
        fyre.conv.load({}, [{
            el: 'livefyre-comments',
            network: "livefyre.com",
            siteId: prop.siteId,
            articleId: articleId,
            signed: false,
            collectionMeta: {
                articleId: articleId,
                url: fyre.conv.load.makeCollectionUrl(),
            }
        }], function () {});
    }

    function loadScript(http, https) {
        if (window.fyre === undefined) {
            var protocol = window.location.protocol;

            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.async = true;
            script.src = (protocol === "https:") ? https : http;
            document.getElementsByTagName('head')[0].appendChild(script);
        }
    }

    api.init = function () {
        var livefyre = $(".livefyre-comments:not(.initialized)");

        livefyre.each(function () {
            var properties = $(this).data("properties");

            if (properties.siteId) {
                loadScript('http://zor.livefyre.com/wjs/v3.0/javascripts/livefyre.js', 'https://cdn.livefyre.com/libs/fyre.conv.load.js');

                setTimeout(function() {
                    initLivefyre(properties);
                }, 1000);

                $(this).addClass("initialized");
            }
        });
    };

    return api;
}(jQuery, document));

XA.register("livefyre", XA.component.livefyre);
