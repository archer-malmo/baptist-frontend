XA.component.equalHeight = (function($, config) {
    var settings = {
        parentSelector: '.row',
        selector: '.equal'
    };

    var api = {};

    function fixHeight() {

        $(settings.parentSelector).each(function() {
            var $elements = $(this).find(" > div > " + settings.selector),
                maxHeight = 0,
                maxPadding = 0,
                $link = null;

            $elements.each(function() {
                $(this).css('min-height', 'inherit');

                $link = $(this).find('.promo-link');
                if ($link.length === 0) {
                    $link = $(this).find('.summary-link');
                }

                if ($link.css('position') == 'absolute') {
                    if ($link.length && $link.height() > maxPadding) {
                        maxPadding = $link.height() + parseInt($link.css('padding-top'), 10) + parseInt($link.css('padding-bottom'), 10) + parseInt($link.css('margin-top'), 10);
                    }
                }

                if ($(this).height() > maxHeight) {
                    maxHeight = $(this).outerHeight(true);
                }

            });

            if (maxHeight > 0) {
                $elements.css({
                    'padding-bottom': maxPadding,
                    'min-height': maxHeight
                });
            }
        });
    }

    api.init = function() {
        $(window).bind('load', function() {
            setTimeout(fixHeight, 0);
        });

        $(window).bind('resize', function() {
            fixHeight();
        });
    };

    return api;

}(jQuery, document));

XA.register("equalHeight", XA.component.equalHeight);