(function($) {
    $.fn.scrollList = function(properties) {

        function ScrollList(elem) {
            this.properties = {
                "elem": elem,
                "visibleItems": 2,
                "autoplay": false,
                "autoplayDelay": 3000,
                "transitionTime": 1000,
                "horizontal": true,
                "circular": false,
                "height": 300,
                "step": 1,
                "mobile": [],
                onSlideChanged: function() {

                }
            }
        }

        ScrollList.prototype.getProperties = function(properties) {
            if (!properties.hasOwnProperty("height")) {
                properties.height = $(this.properties.elem).height();
            }

            if (properties.autoplayDelay < properties.transitionTime) {
                properties.transitionTime = properties.autoplayDelay;
            }

            $.extend(this.properties, properties);
            //add property to remember default visible items number
            this.properties.visibleItemsDefault = this.properties.visibleItems;
        }

        ScrollList.prototype.createNavigation = function(wrapper) {
            wrapper.after("<div class='nav'>" +
            "<div class='prev'><a href='#'>prev</a></div>" +
            "<div class='next'><a href='#'>next</a></div>" +
            "</div>");
        }

        ScrollList.prototype.autoplay = function() {
            var inst = this,
                scrollList = $(this.properties.elem);

            if ((inst.properties.autoplay) && (inst.properties.circular)) {
                var delayTime = this.properties.autoplayDelay,
                    interval = function() {
                        scrollList.find(".nav .next").trigger("click");
                    };

                inst.properties.intervalId = setInterval(interval, delayTime);
                scrollList.hover(
                    function() {
                        clearInterval(inst.properties.intervalId);
                    },
                    function() {
                        inst.properties.intervalId = setInterval(interval, delayTime);
                    }
                );

                $(window).resize(function() {
                    clearInterval(inst.properties.intervalId);
                    inst.properties.intervalId = setInterval(interval, delayTime);
                });
            }
        }


        ScrollList.prototype.replaceMobileProperties = function() {
            var inst = this,
                componentWidth = $(inst.properties.elem).width(),
                visibleItemsChanged = false;


            if (inst.properties.hasOwnProperty("mobile")) {
                $.each(inst.properties.mobile, function(i, item) {
                    if ((componentWidth > item.minWidth) && (componentWidth < item.maxWidth)) {
                        inst.properties.visibleItems = item.visibleItems;
                        visibleItemsChanged = true;
                        return false;
                    }
                });

                if (!visibleItemsChanged) {
                    inst.properties.visibleItems = inst.properties.visibleItemsDefault;
                }
            }
        }

        ScrollList.prototype.animate = function() {

        }

        ScrollList.prototype.horizontalEvents = function(wrapper, scrollItems, scrollSize) {
            var inst = this,
                left = 0,
                itemsLength = scrollItems.children("li").length,
                transitionTime = inst.properties.transitionTime;

            scrollItems.css({
                left: 0
            });
            if (!inst.properties.circular) {
                wrapper.next().find(".prev").addClass("hide");
            }

            wrapper.unbind("touchstart");
            wrapper.on("touchstart", function(e) {
                e.preventDefault();
                clearInterval(inst.properties.intervalId);
                var posX = e.originalEvent.changedTouches[0].pageX;

                $(this).unbind("touchend");
                $(this).on("touchend", function(ev) {
                    ev.preventDefault();
                    var posXEnd = ev.originalEvent.changedTouches[0].pageX;
                    if ((posX - posXEnd) > 100) {
                        wrapper.next().find(".next").trigger("click");
                    } else if ((posX - posXEnd) < -100) {
                        wrapper.next().find(".prev").trigger("click");
                    }
                });
            });

            wrapper.next().find(".next a,.prev a").on("click", function(e) {
                e.preventDefault();
            });

            wrapper.next().find(".next").unbind("click");
            wrapper.next().find(".next").on("click", function() {
                var scrollPosition1 = left,
                    scrollPosition2 = -itemsLength * scrollSize / inst.properties.step + inst.properties.visibleItems * scrollSize / inst.properties.step;

                if (Math.round(scrollPosition1) > Math.round(scrollPosition2)) {
                    left = left - scrollSize;

                    scrollItems.stop().animate({
                        left: left
                    }, transitionTime, function() {
                        inst.properties.onSlideChanged.call(inst);
                    });

                    if (left <= scrollPosition2) {
                        $(this).addClass("hide");
                    }

                    $(this).parent().find(".prev").removeClass("hide");
                } else if (inst.properties.circular) {
                    var itemToRemove = scrollItems.children("li").first();
                    scrollItems.children("li").first().remove();
                    scrollItems.append(itemToRemove);

                    scrollItems.css({
                        left: left + scrollSize
                    });
                    scrollItems.stop().animate({
                        left: left
                    }, transitionTime, function() {
                        inst.properties.onSlideChanged.call(inst);
                    });
                }

            });

            wrapper.next().find(".prev").unbind("click");
            wrapper.next().find(".prev").on("click", function() {
                if (Math.round(left + scrollSize) <= 0) {
                    left = left + scrollSize;

                    scrollItems.stop().animate({
                        left: left
                    }, transitionTime, function() {
                        inst.properties.onSlideChanged.call(inst);
                    });

                    if (Math.round(left) === 0) {
                        $(this).addClass("hide");
                    }

                    $(this).parent().find(".next").removeClass("hide");
                } else if (inst.properties.circular) {
                    var itemToRemove = scrollItems.children("li").last();
                    scrollItems.children("li").last().remove();
                    scrollItems.prepend(itemToRemove);

                    scrollItems.css({
                        left: -scrollSize
                    });
                    scrollItems.stop().animate({
                        left: left
                    }, transitionTime, function() {
                        inst.properties.onSlideChanged.call(inst);
                    });
                }

            });
        }

        ScrollList.prototype.generateHorizontalScroll = function(prop) {
            var inst = this,
                scrollSize,
                scrollWrapper = $(inst.properties.elem).find(".scroll-wrapper"),
                scrollItems = scrollWrapper.children("ul"),
                scrollItem = scrollItems.children("li"),
                wrapperWidth = scrollWrapper.width(),
                itemPaddingLeft = parseInt(scrollItem.css("padding-left")),
                itemPaddingRight = parseInt(scrollItem.css("padding-right")),
                itemMarginLeft = parseInt(scrollItem.css("margin-left")),
                itemMarginRight = parseInt(scrollItem.css("margin-right")),
                itemBorderWidth = parseInt(scrollItem.css("border-left-width")),
                itemWidth = (wrapperWidth - inst.properties.visibleItems * itemPaddingLeft -
                    inst.properties.visibleItems * itemPaddingRight) / inst.properties.visibleItems -
                    2 * itemBorderWidth - itemMarginRight - itemMarginLeft;


            $(window).on('load', function() {
                scrollWrapper.height(scrollItem.outerHeight());
            });

            $(window).on('resize', function() {
                scrollWrapper.height(scrollItem.outerHeight());
            });

            scrollItem.width(itemWidth);

            if (!prop.resize) {
                this.createNavigation(scrollWrapper);
            }

            scrollSize = itemWidth + itemPaddingLeft + itemPaddingRight + itemBorderWidth * 2 + itemMarginRight + itemMarginLeft;
            scrollSize = scrollSize * inst.properties.step;

            this.horizontalEvents(scrollWrapper, scrollItems, scrollSize);
        }




        ScrollList.prototype.generateVerticalScroll = function(prop) {
            var inst = this,
                scrollWrapper = $(inst.properties.elem).find(".scroll-wrapper"),
                scrollSize,
                scrollItems = scrollWrapper.children("ul"),
                scrollItem = scrollItems.children("li"),
                itemPaddingTop = parseInt(scrollItem.css("padding-top")),
                itemPaddingBottom = parseInt(scrollItem.css("padding-bottom")),
                itemMarginTop = parseInt(scrollItem.css("margin-top")),
                itemMarginBottom = parseInt(scrollItem.css("margin-bottom")),
                itemBorderHeight = parseInt(scrollItem.css("border-top-width")),
                listPadding = 80,
                itemHeight = (this.properties.height - inst.properties.visibleItems * itemPaddingTop -
                    inst.properties.visibleItems * itemPaddingBottom) / inst.properties.visibleItems -
                    itemBorderHeight * 2 - itemMarginTop - itemMarginBottom;


            $(window).on('load', function() {
                scrollWrapper.height(scrollItem.outerHeight() + listPadding);
                scrollItem.height(scrollItem.outerHeight() / inst.properties.visibleItems);
            });

            $(window).on('resize', function() {
                scrollWrapper.height(scrollItem.outerHeight() + listPadding);
                scrollItem.height(scrollItem.outerHeight() / inst.properties.visibleItems);
            });

            if (!prop.resize) {
                this.createNavigation(scrollWrapper);
            }

            //itemHeight + itemPaddingTop + itemPaddingBottom + 2 * itemBorderHeight + itemMarginTop + itemMarginBottom;
            //scrollSize = scrollSize * inst.properties.step;

            $(window).on('load', function() {
                scrollSize = (scrollItem.outerHeight() / inst.properties.visibleItems) * inst.properties.step;
                inst.verticalEvents(scrollWrapper, scrollItems, scrollSize);
            });
        }


        ScrollList.prototype.verticalEvents = function(wrapper, scrollItems, scrollSize) {
            var inst = this,
                top = 0,
                itemsLength = scrollItems.children("li").length,
                transitionTime = inst.properties.transitionTime;

            scrollItems.css({
                top: 0
            });
            if (!inst.properties.circular) {
                wrapper.next().find(".prev").addClass("hide");
            }

            wrapper.next().find(".next a,.prev a").on("click", function(e) {
                e.preventDefault();
            });

            wrapper.unbind("touchstart");
            wrapper.on("touchstart", function(e) {
                e.preventDefault();
                clearInterval(inst.properties.intervalId);
                var posY = e.originalEvent.changedTouches[0].pageY;

                $(this).unbind("touchend");
                $(this).on("touchend", function(ev) {
                    ev.preventDefault();
                    var posYEnd = ev.originalEvent.changedTouches[0].pageY;
                    if ((posY - posYEnd) > 100) {
                        wrapper.next().find(".next").trigger("click");
                    } else if ((posY - posYEnd) < -100) {
                        wrapper.next().find(".prev").trigger("click");
                    }
                });
            });

            wrapper.next().find(".next").unbind("click");
            wrapper.next().find(".next").on("click", function() {
                var scrollPosition1 = top,
                    scrollPosition2 = scrollSize / inst.properties.step; // + inst.properties.visibleItems * scrollSize / inst.properties.step;

                if (Math.round(scrollPosition1) > Math.round(scrollPosition2)) {
                    top = top - scrollSize;

                    scrollItems.stop().animate({
                        top: top
                    }, transitionTime, function() {
                        inst.properties.onSlideChanged.call(inst);
                    });

                    if (Math.round(top) <= Math.round(scrollPosition2)) {
                        $(this).addClass("hide");
                    }

                    $(this).parent().find(".prev").removeClass("hide");
                } else if (inst.properties.circular) {
                    var itemToRemove = scrollItems.children("li").first();

                    scrollItems.css({
                        top: top + scrollSize
                    });

                    scrollItems.stop().animate({
                        top: top
                    }, transitionTime, function() {
                        inst.properties.onSlideChanged.call(inst);
                    });

                    scrollItems.children("li").first().remove();
                    scrollItems.append(itemToRemove);
                }
            });

            wrapper.next().find(".prev").unbind("click");
            wrapper.next().find(".prev").on("click", function() {

                if (Math.round(top + scrollSize) <= 0) {
                    top = top + scrollSize;

                    scrollItems.stop().animate({
                        top: top
                    }, transitionTime, function() {
                        inst.properties.onSlideChanged.call(inst);
                    });
                    if (Math.round(top) === 0) {
                        $(this).addClass("hide");
                    }

                    $(this).parent().find(".next").removeClass("hide");
                } else if (inst.properties.circular) {
                    var itemToRemove = scrollItems.children("li").last();

                    scrollItems.css({
                        top: top - scrollSize
                    });

                    scrollItems.stop().animate({
                        top: top
                    }, transitionTime, function() {
                        inst.properties.onSlideChanged.call(inst);
                    });


                    scrollItems.children("li").last().remove();
                    scrollItems.prepend(itemToRemove);

                }
            });
        }

        ScrollList.prototype.disableEmptySlides = function () {
            var emptyPlaceholders = this.properties.elem.find('.slide .scEmptyPlaceholder'),
            	placeholderCodeTag,
            	$placeholder,
            	placeholderId,
            	placeholderKey,
            	editFrame,
            	chromeKey,
            	h;
            _.each(emptyPlaceholders, function(placeholder) {
                $placeholder = $(placeholder);
                placeholderId = $placeholder.attr("sc-placeholder-id");

                //disable edit frames
                placeholderCodeTag = $placeholder.siblings("code[chrometype='placeholder'][id='"+placeholderId+"']");
                try {
            		placeholderKey = placeholderCodeTag.attr("key");
            		if(typeof(placeholderKey) === "string"){
            			editFrame = Sitecore.PageModes.ChromeManager.chromes().filter(function(chrome){
        					chromeKey = chrome.openingMarker();
	                        if(!chromeKey){
	                            return false;
	                        }

	                        chromeKey = chromeKey.attr("key");
	                        return typeof(chromeKey) !== "undefined" && chromeKey.indexOf(placeholderKey) === 0;
            			});

            			editFrame[0].type.chrome.data.custom.editable = "false";
            		}
            		else {
            			throw 666;
            		}
                }
                catch(e){ //fallback - delete code tags to prevent inserting into placeholders
            		$placeholder.siblings("code[chrometype='placeholder']").remove();
            		console.log("Could not disable editing for placeholder",e);
                }

            	//adjust height, and remove placeholder "dashed" background
                h = $placeholder.height() + "px";
                $placeholder.removeClass("scEmptyPlaceholder");
                $placeholder.css("height",h);

                //set edit here text
                $placeholder.append($("<p>[No components in slide]</p>"));
            });
        };


        var scrollList = new ScrollList(this);
        scrollList.getProperties(properties);
        scrollList.replaceMobileProperties();
        scrollList.disableEmptySlides();

        if (scrollList.properties.circular) {
            $(this).addClass("circular");
        }

        if (scrollList.properties.horizontal) {
            $(this).addClass("horizontal");
            scrollList.generateHorizontalScroll({
                resize: false
            });

            $(window).resize(function() {
                scrollList.replaceMobileProperties();
                scrollList.generateHorizontalScroll({
                    resize: true
                });
            });
        } else {
            $(this).addClass("vertical");
            scrollList.generateVerticalScroll({
                resize: false
            });

            $(window).resize(function() {
                scrollList.replaceMobileProperties();
                scrollList.generateVerticalScroll({
                    resize: true
                });
            });
        }

        scrollList.autoplay();


        return this;
    }
})(jQuery);


XA.component.scrollList = (function($, document) {
    var api = {};
    api.init = function() {
        var scrollList = $(".scroll-list");

        scrollList.each(function() {
            var properties = $(this).data("properties");
            $(this).scrollList(properties);
        });
    };

    return api;
}(jQuery, document));

XA.register("scrollList", XA.component.scrollList);