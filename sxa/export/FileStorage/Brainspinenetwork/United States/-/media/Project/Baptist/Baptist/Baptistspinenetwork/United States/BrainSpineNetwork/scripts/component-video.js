XA.component.video = (function($, document) {

    var api = {};

    function checkSize(video) {
        var videoWidth = video.width();
        video.removeClass("video-small hide-controls");

        if ((videoWidth < 481) && (videoWidth >= 321)) {
            video.addClass("video-small");
        } else if (videoWidth < 321) {
            video.addClass("hide-controls");
        }
    }

    function checkIE11() {
        if (navigator.userAgent.match(/Trident\/7.0/)) {
            return true;
        }

        return false;
    }

    function initVideo(video, properties) {
        var content = video.find("video");

        if(!content.length){
            return;
        }

        if (!movieTracker) {
            movieTracker = XAContext.Tracking.Movies($);
        }

        var callback = function(mediaElement, domObject) {
            mediaElement.movieName = 'Movie';
            movieTracker.register({
                name: 'Movie',
                api: mediaElement,
                trackerId: 'mejs',
                completedTime: null
            });

            $(mediaElement).on("ended", function() {
                if (properties.fromPlaylist) {
                    $(properties.playlist).trigger("change-video");
                }
            });
        }

        $.extend(properties, {
            "plugins": ['youtube', 'flash', 'silverlight'],
            "silverlightName": 'silverlightmediaelement.xap',
            "success": callback
        });

        if (checkIE11()) {
            properties = null;
        }

        return new MediaElementPlayer(content, properties);
    }

    api.initVideoFromPlaylist = function(video, playlist) {
        var properties = $(video).data("properties");

        $.extend(properties, {
            "fromPlaylist": true,
            "playlist": playlist
        });

        return initVideo(video, properties);
    }


    api.init = function() {
        if(XA.component.hasOwnProperty("playlist")){
            XA.component.playlist.init();
        }

        var video = $(".video.component:not(.initialized)");

        video.each(function() {
            var properties = $(this).data("properties");

            initVideo($(this), properties);
            checkSize($(this));

            $(window).resize(function() {
                checkSize($(this));
            });

            $(this).addClass("initialized");
        });

        $(document).on('mozfullscreenchange', function() {
            setTimeout(function() {
                $(window).resize();
            }, 200); //mozilla bug fix
        });
    };

    return api;
}(jQuery, document));

XA.register("video", XA.component.video);