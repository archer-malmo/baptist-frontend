XA.component.toggle = (function ($, document) {

    var toogleEvents = {
            focus: function () {
                $(this).addClass("show");
            },
            blur: function () {                
                $(this).removeClass("show");
            }

        };

    function initEvents(header) {
        header.on("mouseover", toogleEvents.focus);
        header.on("mouseleave", toogleEvents.blur);
        header.on("focus", toogleEvents.focus);
        header.on("blur", toogleEvents.blur);
        header.on("keyup", function (e) {
            if (e.keyCode == 13) {
                $(this).click();
            }
        });
    };
    
    var api = {};

    api.init = function () {
        var toogleHeaders = $(".toggle-header:not(.initialized)");
        for (var i = 0; i < toogleHeaders.length; i++) {
            var header = $(toogleHeaders[i]);
            initEvents(header);
            header.addClass("initialized");
        };
    };

    return api;
}(jQuery, document));

XA.register("toggle", XA.component.toggle);