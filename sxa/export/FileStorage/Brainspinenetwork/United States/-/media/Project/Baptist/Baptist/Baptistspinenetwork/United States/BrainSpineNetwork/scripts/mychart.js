//------------------------------------------------------------------
// *lchriste 251276 11/12 
// JavaScript included in signup_login.js below this line
//------------------------------------------------------------------
// NAME: 		WPClearFields
// DESCRIPTION: clears out credentials after login
// REVISION HISTORY:
// *lchriste 269056 05/13 created
function WPClearFields() {
	var loginInput = document.getElementById("Login");
	var passwordInput = document.getElementById("Password");
	loginInput.value = '';
	passwordInput.value = '';
}


//------------------------------------------------------------------
// NAME:         WPSetJsEnabled
// DESCRIPTION:  sets the jsenabled flag when logging in.
// PARAMETERS:
// RETURNS:      passes the jsenabled flag
// REVISION HISTORY:
//  *mehmet 06/10 178030 Changed pop up error messages with inline error messages & error messages on top of the page.
//  *rscott 02/11 196417 Add WPEnableInputs() to support requiring JavaScript for login / signup.
//  *lchriste 11/12 251276 just set the jsenabled flag for the login box
//------------------------------------------------------------------
/* ***** Function that makes sure that login, password are entered in the login form  ***** */
function WPSetJsEnabled() {
  var loginForm;
  loginForm = document.getElementById("MyChartLoginForm");
  if (loginForm) {
    loginForm.jsenabled.value = "1";
  }
  return true;
}
//-----------------------------------------------------------------
// NAME: WPSetFocusOnLogin
//  *lchriste 11/12 251276 change the onBodyLoad to just set the focus to the first field
// ***** 'WPSetFocusOnLogin' fn. for the login box **** 
//-----------------------------------------------------------------
function WPSetFocusOnLogin() {
  var loginForm = document.getElementById("MyChartLoginForm");


  if (loginForm) {
    //set focus on the first blank value
    if (loginForm.Login.value == '')
    {
      loginForm.Login.focus();
    }
    else if (loginForm.Password.value == '')
    {
      loginForm.Password.focus();
    } else {
      loginForm.Submit.focus();
    }
  }
  return true;
}
//-------------------------------------------------------------------
// NAME: WPEnableInputs
// DESCRIPTION: Enable inputs on the page so user can proceed. Used to enforce JavaScript being enabled.
// CALLED BY: Onload action in login-related pages, including signup, etc...
//-------------------------------------------------------------------
function WPEnableInputs() {
  var tmpElmnt, elmntArr, indx;
  //Hide the alert message
  tmpElmnt = document.getElementById('nojavascript');
  if (tmpElmnt !== null) {
    tmpElmnt.className = tmpElmnt.className + ' hidden';
    WPHideElement(tmpElmnt);
  }

  //Enable all the disabled fields
  elmntArr = document.getElementsByTagName('INPUT');
  if (elmntArr === null) { return; }
  for (indx = 0; indx < elmntArr.length; indx = indx + 1) {
    tmpElmnt = elmntArr[indx];
    if (tmpElmnt !== null && WPContainsClassName(tmpElmnt, 'jsreq')) {  //Only do this for fields marking as requiring JavaScript. Realistically, this should be everything.
      tmpElmnt.removeAttribute('disabled');
      WPRemoveClassNames(tmpElmnt, 'disabledfield');  //For text fields
      WPRemoveClassNames(tmpElmnt, 'disabled');       //For buttons
    }
  }
}	
//------------------------------------------------------------------
// *lchriste 251276 11/12 
// Javascript in CommonUtils.js below this line
//------------------------------------------------------------------

//------------------------------------------------------------------
//NAME: WPShowElement
//DESCRIPTION: Show the given element by removing the 'hidden' CSS class name.
//              Should probably be given used instead of showItem.
//PARAMETERS:
//   element - Reference to the element to show.
//   elmntId - If 'element' is null, then use this ID to look up the element.
//------------------------------------------------------------------
function WPShowElement(element, elmntId) {
  if (element === null) {
    if (elmntId !== null && elmntId.length > 0) {
      element = document.getElementById(elmntId);
    }
    if (element === null) { return; }
  }
  WPRemoveClassNames(element, 'hidden');
  return;
}
//-------------------------------------------------------------------
// NAME: WPHideElement
// DESCRIPTION: Hide the given element by adding the 'hidden' CSS class name.
// PARAMETERS:
//   element - Reference to the element to hide.
//   elmntId - If 'element' is null, then use this ID to look up the element.
// HISTORY:
//   *rscott 01/11 194574 - Refactored to use CSS instead of directly modifying the display property.
//-------------------------------------------------------------------
function WPHideElement(element, elmntId) {
  if (element === null) {
    if (elmntId !== null && elmntId.length > 0) {
      element = document.getElementById(elmntId);
    }
    if (element === null) { return; }
  }
  WPRemoveClassNames(element, 'hidden'); //Remove it first, in case it's already hidden.
  element.className = element.className + ' hidden';
  return;
}
//--------------------------------------------------------------------
// NAME: WPRemoveClassNames
// DESCRIPTION: Remove all instances of the given classNames from the given element's 'class' attribute.
// PARAMETERS:
//    element - Reference to the element
//    classNms - Comma-delimited list of class name to be removed.
// CALLED BY:  WPShowElement, WPHideElement, activateField, deactivateField
// HISTORY:
//   *mborofsk 4/12 229725 - improved sanity checking
// *dhe 02/12 228369 fix documentation
//--------------------------------------------------------------------
function WPRemoveClassNames(element, classNms) {
  var classNmArray, indx, expr, newClassName;
  if (!element || !classNms || element.className.length < 1 || classNms.length < 1) { return; }  
  classNmArray = classNms.split(','); //Split arguments on comma
  newClassName = element.className;
  for (indx = 0; indx < classNmArray.length; indx = indx + 1) {
    expr = new RegExp('(^|\\s+)' + classNmArray[indx] + '(\\s+|$)', 'g');
    newClassName = newClassName.replace(expr, ' ');
  }
  element.className = newClassName;
}
//---------------------------------------------------------------------
// NAME: WPContainsClassName
// DESCRIPTION: Returns whether an element belongs to a specified CSS class.
// PARAMETERS:
//    element - The element to test for.
//    classNm - Name of the css class to test against.
// RETURNS: true if element belongs to the CSS class classNm; false otherwise.
// CALLED BY:
// HISTORY:
//   *mborofsk 4/12 229725 - improved sanity checking
// *dhe 02/12 228369 return a boolean. Previously: return either a boolean, an array, or null.
//---------------------------------------------------------------------
function WPContainsClassName(element, classNm) {
  var expr;
  if (!element || typeof classNm !== 'string' || classNm.length < 1) { return false; }
  if (typeof element.className !== 'string' || element.className.length < 1) { return false; }
  expr = new RegExp('(^|\\s+)' + classNm + '(\\s+|$)', 'g'); //Check for the classname at the beginning or end of the string or surrounded by whitespaces.
  if (!expr) { return false; }
  return element.className.match(expr) != null;
}
WPEnableInputs();WPSetFocusOnLogin();


$(function(){
  //$("#MyChartLoginPanel").hide();
});