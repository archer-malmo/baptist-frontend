﻿$(function () {
  /*$('a#location-change').click(function (event) {
    event.preventDefault();
    $('.option-list').toggleClass('active');
    if ($('.option-list').hasClass('active')) {
      $('a#location-change').text('cancel');
    } else {
      $('a#location-change').text('change');
    }
  });*/

  var changelocationlistener = function (event) {
    var clientID = $(this).val();
    var updateurl = '/api/sitecore/ERWait/HeaderLocationOverride?clientID=' + clientID;
    console.log(clientID);


    $.ajax({
      url: updateurl,
      cache: false,
      method: 'GET',
      success: function (data) {
        $('div#header-er-wait').replaceWith(data);

        //$('.option-list').toggleClass('active');
        $('a#location-change').bind('click', function (event) {
          event.preventDefault();
          //$('.option-list').toggleClass('active');
        });
      },
      error: function (data) {
        console.log(data);
        //$('.option-list').toggleClass('active');
      }
    });

    var optionslisturl = '/api/sitecore/ERWait/HeaderUpdatedListOptions?clientID=' + clientID;

    $.ajax({
      url: optionslisturl,
      cache: false,
      method: 'GET',
      success: function (data) {
        //console.log(data);
        $('div#location-option-list div.modal-body').html(data);

        $('button[rel="change-default-location"]').bind('click', changelocationlistener);
        //console.log('bound');
      }, error: function (data) {
        console.log(data);
      }
    });

    $('#location-option-list').modal('hide');
  };

  $('button[rel="change-default-location"]').bind('click', changelocationlistener);

  $('a[data-change-location]').click(function (event) {
    event.preventDefault();
    $('#location-option-list').modal('toggle');
  });
})