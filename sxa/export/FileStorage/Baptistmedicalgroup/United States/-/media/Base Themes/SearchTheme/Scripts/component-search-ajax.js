XA.component.search.ajax = (function ($, document) {

    var APIModel = Backbone.Model.extend({
        getData: function (properties) {
            Backbone.ajax({
                dataType: "json",
                url: properties.url,
                success: function(data){
                    properties.callback(data);
                }
            });
        }
    });
    return new APIModel();

}(jQuery, document));