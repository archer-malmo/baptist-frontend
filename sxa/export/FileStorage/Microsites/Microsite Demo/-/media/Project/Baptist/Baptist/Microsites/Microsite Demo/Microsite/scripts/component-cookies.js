XA.component.cookie = (function ($, document) {

    var cookieWarning,
        cookiePosition = "top",
        submitTag,
        api = {
            settings: {
                submit: $("submit"),
                defaultPos: function () {
                    if (cookieWarning.hasClass("cookie-bottom")) {
                        cookiePosition = "bottom";
                        cookieWarning.css("display", "block");
                    } else {
                        cookiePosition = "top";
                        cookieWarning.css("display", "block");
                    }
                }
            },

            init: function () {
                cookieWarning = $(".cookie-warning:not(.initialized)");
                var isEmpty = cookieWarning.find(".component-content").children().length;

                if (isEmpty) {
                    var s = this.api.settings;
                    s.defaultPos();
                    submitTag = $(".submit a");
                    submitTag.attr("tabIndex",1);
                    submitTag.attr("title","Allow cookies");
                    if (cookiePosition == "top") {
                        // Serve from top of page
                        submitTag.on('click', function (e) {
                            e.preventDefault();
                            XA.cookies.createCookie("cookie-warning", 1, 365);
                            cookieWarning.animate({
                                top: "-300px"
                            }, 1000, function () {
                                cookieWarning.remove();
                            });
                        });
                    } else {
                        // Serve from bottom of page
                        submitTag.on('click', function (e) {
                            e.preventDefault();
                            XA.cookies.createCookie("cookie-warning", 1, 365);
                            cookieWarning.animate({
                                bottom: "-300px"
                            }, 1000, function () {
                                cookieWarning.remove();
                            });
                        });
                    }
                }

                cookieWarning.addClass("initialized");
            }
        };

    return api;

})(jQuery, document);


XA.register("cookie", XA.component.cookie);