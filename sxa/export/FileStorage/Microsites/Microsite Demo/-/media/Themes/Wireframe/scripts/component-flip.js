XA.component.flip = (function ($, document) {
    var api = {};

    function detectMobile() {
        return 'ontouchstart' in window;
    }

    function disableEmptyFlips($flip) {
        var emptyPlaceholders = $flip.find(".scEmptyPlaceholder"),
        i, length, h,
        parent,
        $placeholder,
        placeholderId,
        placeholderKey,
        editFrame,
        chromeKey,
        h;

        for (i = 0, length = emptyPlaceholders.length; i < length; i++) {
            $placeholder = $(emptyPlaceholders[i]);
            placeholderId = $placeholder.attr("sc-placeholder-id");

            //disable edit frames
            placeholderCodeTag = $placeholder.siblings("code[chrometype='placeholder'][id='" + placeholderId + "']");
            try {
                placeholderKey = placeholderCodeTag.attr("key");
                if (typeof (placeholderKey) === "string") {
                    editFrame = Sitecore.PageModes.ChromeManager.chromes().filter(function (chrome) {
                        chromeKey = chrome.openingMarker();
                        if (!chromeKey) {
                            return false;
                        }

                        chromeKey = chromeKey.attr("key");
                        return typeof (chromeKey) !== "undefined" && chromeKey.indexOf(placeholderKey) === 0;
                    });

                    editFrame[0].type.chrome.data.custom.editable = "false";
                }
                else {
                    throw 666;
                }
            }
            catch (e) { //fallback - delete code tags to prevent inserting into placeholders
                $placeholder.siblings("code[chrometype='placeholder']").remove();
                console.log("Could not disable editing for placeholder", e);
            }

            //adjust height, and remove placeholder "dashed" background
            parent = $placeholder.parents(".slide-heading");
            if (parent.length !== 0) {
                h = parseInt(parent.css("font-size")) * 2 || ($placeholder.height() / 3);
                $placeholder.append($("<p>[No components in title section]</p>"));
            }
            else {
                h = $placeholder.height();
                $placeholder.append($("<p>[No components in content section]</p>"));
            }
            h += "px";
            $placeholder.removeClass("scEmptyPlaceholder");
            $placeholder.css("height", h);

            //set edit here text

        }
    }

    api.init = function () {
        var flip = $('.flip:not(.initialized)');

        flip.each(function () {
            $flipModule = $(this).find(".flipsides");
            if ($(this).hasClass('flip-hover') && (!detectMobile())) {
                $(this).hover(function () {
                    $(this).addClass('active');
                },
                    function () {
                        $(this).removeClass('active');
                    });
            } else {
                $(this).on('click', function () {
                    $(this).toggleClass('active');
                });
            }

            $(this).addClass('initialized');
            disableEmptyFlips($flipModule);
        });
    };
    return api;

}(jQuery, document));


XA.register('flip', XA.component.flip);