# Baptist Frontend

## Background info

All 3 Baptist sites are built in the Sitecore CMS, so the frontend development workflow is based on the Sitecore Experience Accelerator (SXA) workflow. Understanding SXA is helpful to understanding how this repo works. You can find the documentation for SXA here: https://doc.sitecore.net/sitecore_experience_accelerator/

Essentially, you will work on a local, static HTML version of the site (exported from Sitecore via the "Creative Exchange"). Then you can work on the theme files, located under the `assets` folder. Grunt will process the files so you can preview your changes locally. When you commit and push a code change, the project will run through a build/deployment process. When that's complete, you will need to import the theme in Sitecore using the Creative Exchange, then publish the site theme. Read the **Development Workflow** section below for more details.

## Initial setup

After cloning the repo, follow these steps:

 - Run `yarn install` (or `npm install` if you aren't using yarn)
 - Make sure you have a webserver you can run locally to preview the HTML files in your browser:
    - You can use MAMP/WAMP or [the built-in PHP webserver](http://php.net/manual/en/features.commandline.webserver.php).
    - You can also create an entry in your hosts file that maps local.dev (or something similar) to 127.0.0.1.
    - The root directory of the server will depend on what you need to work on:
       - If you need to work on a theme or page for a particular site, run the server in one of the site directories under `/export/FileStorage` where there's an `index.html` file, for example `/export/FileStorage/<site name>/United States/`
       - If you need to work on a custom component that will be generated dynamically by backend code, run the server in the `custom` directory. Static versions of custom components are built here, and when they're ready, they get transferred to the other Baptist repo that contains the Visual Studio project. Look for the line that says `BEGIN CUSTOM CODE`.

## Development workflow
 - Start your local dev server
 - Consider updating the content in the repo with the latest content from Sitecore. Depending on what you're working on, this may or may not be necessary. If you need to style a new piece of content that was recently created in the CMS, you will need to do this step. See the section below called **Creative Exchange export** for details.
 - Decide which theme you need to work on. Each Baptist site has its own unqiue theme. The basic template is almost indentical, but the color scheme is different. To see which theme you're currently working on, look at the `_current-theme.sass` file. You can change the current theme (i.e. the theme processed by the grunt watch task) using one of the following commands (if grunt watch is already running, make sure you restart the task after changing themes):
    - `grunt use-theme-bol`
    - `grunt use-theme-bcc`
    - `grunt use-theme-bmg`
 - Run `grunt` to start watching the project directory for changes
 - After making your changes, commit and push your code to Beanstalk. The build/deployment process will begin automatically within a few minutes. First, the project will get built by TeamCity. Next, the code will get deployed to the server by Octopus. When these 2 steps are done, you will need to perform 2 more steps manually. See the section below called **Creative Exchange import** for details. You can view the progress of the build/deployment process, along with any error messages, at the following dashboard URLs:
    - **TeamCity:** http://tc.baptist.amdevel.com
    - **Octopus:** http://octopus.baptist.amdevel.com
 - If you need to to manually re-build all of the assets for some reason, run `grunt full-build`

## Branches

There is currently only one branch, `master`, which can be deployed to the staging, UAT, or production environments (see below for environment details).

## Deployments/Environments

There is a staging version of all 3 sites:

 - baptist.amdevel.com
 - cancer.baptist.amdevel.com
 - bmg.baptist.amdevel.com

There is also a UAT version of all 3 sites. This version is specifically for a third-party vendor to test the site for ADA compliance.

 - uat.baptist.amdevel.com
 - uat_cancer.baptist.amdevel.com
 - uat_bmg.baptist.amdevel.com

Both staging and UAT use the `master` branch. To deploy to staging instead of UAT (or vice versa), select the appropriate publish target when you publish the theme.

And here are the production URLs:

 - baptistonline.org
 - baptistcancercenter.com
 - baptistdoctors.org

There is also an authoring environment for accessing the production content management system. This is available at:

- cm.baptist.amdevel.com

The production environment also uses the `master` branch. To deploy code to production, first you need to log into Octopus and create a new release. Deploy this release to the authoring environment first. If that's successful, click the button to promote to production (inside the authoring environment).

Sitecore will automatically minify and concatenate all the CSS and JS into a single file, `optimized-min.css` and `optimized-min.js`. This gets generated on the server automatically by the CMS. If you make a change to any CSS or JS and you're not seeing your changes on production, you might need to browse to the Media Library in the Content Editor and delete the optimized-min item. Then load a page on the production URL to force Sitecore to regenerate that file.

**NOTE:** After deploying a release to production, the prod site will become very slow (essentially unresponsive) for several minutes after the deployment. For this reason, avoid deploying to prod too frequently. Test your changes as much as possible locally or on staging and consolidate a bunch of small changes into one big release. For some smaller changes--updating a single image file, for example--it's usually faster to log into the production CMS, browse to that file in the Media Library, and reattach the file to the media item, rather than doing the full deployment process.

To edit content within the CMS on production, log in to cm.baptist.amdevel.com. When you publish from this environment, the content changes go straight to production.

## Miscellaneous
 - Do not change any content or markup structure inside any of the HTML files under the `export` folder. This is not supported by the Creative Exchange importer and can break the site. The only change you can make here is adding custom CSS classes to certain elements where it says `add-your-css-classes-here` in the `class` attribute. Make sure you add your custom classes **after** that.
 - Do not use IDs for styling. This can create conflicts with the IDs used internally by Sitecore. Use classes rather than IDs in your CSS selectors.
 - Some styles will never show up correctly inside the Experience Editor's preview. It's usually better to keep a separate browser or incognito window open to preview styles outside Sitecore.
 - If your style changes don't appear after a while, even after a hard refresh in your browser, you might need to clear the Sitecore cache. This can be done at http://baptist.amdevel.com/sitecore/admin/cache.aspx.

### Creative Exchange export

Log into Sitecore, browse to the site in the content editor, and open the experience editor on any page of the site you're currently working on. Go to the experience editor tab in the top toolbar, and click **Export**. A modal window will appear with export options, but the default options are usually okay so you can click next and the export will begin. **This process will take about 15 minutes.** When it's complete, Sitecore might report some errors or warnings from the export process. These are usually safe to ignore. Download and extract the ZIP file it creates. Copy those files and paste them into the appropriate site folder under `/export/FileStorage`. Your computer will probably ask if you want to overwrite the files and folders in there--click yes and overwrite everything.

**Important note:** After copying the new files into the `export` folder, you will need to look for any files/folders with extremely long names. These will break the build/deployment process due to Windows' 260 character limit on file path length. This has a tendency to happen with the PDF files in the `Documents` folder in the media library. It's probably safe to delete all PDF files to avoid this issue, since they usually aren't relevant to local development. But this issue could occur with other files, so keep an eye out for that. You will need to log into TeamCity and/or Octopus to investigate any build/deployment issues.

### Creative Exchange import

Log into Sitecore, browse to the site in the content editor, and open the experience editor on any page of the site you're currently working on. Go to the experience editor tab in the top toolbar, and click **Import**. A modal window will appear with import options. Select **Folder on server** in this window, then click next and the import will begin. **This process will take a few minutes.** When it's complete, Sitecore will show a summary of which files got updated and it might report some errors or warnings (which are usually safe to ignore). When the import is complete, the last step is to publish the theme files. On the right side of the Experience Accelerator tab, click the button that says **Publish**, then, select **Site Theme** in that drop down menu and go through the usual publish process.

**NOTE:** After a lot of testing, we eventually realized the export/import process is slow and buggy. To speed things up, we've tweaked the deployment settings so now main.css gets deployed directly to the server, avoiding the import process.

To take advantage of this faster deployment process, go here:
http://octopus.baptist.amdevel.com/app#/projects/staging-sxa-theme-deploy

Then click the deploy button underneath "Author cm.baptist.amdevel.com", then "Production CD" (in that order). Then, in the Sitecore content editor, delete the existing optimized-min item in the theme's style folder. Then load the site to make sure the optimized-min file regenerates correctly. Note that this process only works with style changes to the main.css file.

This improved process means you don't have to do a Creative Exchange import or publish the theme if you only make a change to one of the SASS files. You might still need to do the full export/import process for other types of changes, however. If you decide to do this, make sure you **always do a full export first to update the content in the repo before doing the import**. If you don't do this, certain types of changes that were done directly in the CMS (such as adding a class or tweaking the grid settings of a component) will get blown away.