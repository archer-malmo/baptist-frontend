XA.component.navigation = (function ($, document) {

    var timeout = 200,
        timer = 0,
        submenu,
        dropDownEvents = {
            show: function (sm) {
                this.debounce();
                if (submenu) {
                    submenu.parent().removeClass("show");
                    submenu.parent().attr("aria-expanded", false);
                }
                submenu = sm;
                submenu.parent().addClass("show");
				submenu.parent().attr("aria-expanded", true);
            },
            debounce: function () {
                if (timer) {
                    clearTimeout(timer);
                    timer = null;
                }
            },
            hide: function () {
                if (submenu) {
                    submenu.parent().removeClass("show")
					submenu.parent().attr("aria-expanded", false);
                }
            },
            queueHide: function () {
                timer = setTimeout(function() {
                    dropDownEvents.hide();
                }, timeout);
            },
            focus: function () {
                $(this).parent().siblings().removeClass("show");
                $(this).parent().addClass("show");
				$(this).parent().attr("aria-expanded", true);
            },
            blur: function () {
                if ($(this).parent().is(".last")) {
                    $(this).parents(".rel-level1").removeClass("show");
                    $(this).parents(".rel-level1").attr("aria-expanded", false);
                }
            }

        };

    function dropDownNavigation(navi) {
		if (currentTheme === 'baptistOnline') {
			dropDownSelector = '.rel-level1:nth-child(3)';
		} else if (currentTheme === 'cancerCenter') {
			dropDownSelector = '.rel-level1:nth-child(3), .rel-level1:nth-child(4)';
		} else if (currentTheme === 'medicalGroup') {
			dropDownSelector = '.rel-level1:nth-child(1)';
		}
        navi.on("mouseover", dropDownSelector, function () {
            $(this).removeClass("show");
            $(this).siblings().removeClass("show");
            var elem = $(this).find("ul");
            dropDownEvents.show(elem);
        });
        navi.on("mouseleave", dropDownSelector, dropDownEvents.queueHide);
        navi.on("focus", dropDownSelector, dropDownEvents.focus);
        navi.on("blur", ".rel-level2 a", dropDownEvents.blur);

        navi.find(".rel-level1").each(function () {
            if ($(this).find("ul").length) {
                $(this).addClass("submenu");
            }
        });

		navi.on('keydown', dropDownSelector + ' > div > a', function(event) {
			var keyPressed = event.keyCode || event.which;
			var parentMenuItem = $(this).closest('li').children('.submenu-container');
			if (keyPressed === 32) { //Spacebar
				event.preventDefault();
				if (parentMenuItem.hasClass('show')) {
					dropDownEvents.hide();
				} else {
					parentMenuItem.removeClass("show");
					parentMenuItem.siblings().removeClass("show");
					var elem = parentMenuItem.children("ul");
					dropDownEvents.show(elem);
				}
			}
		});

		navi.on('keydown', dropDownSelector + ' ul', function(event) {
			var keyPressed = event.keyCode || event.which;
			if (keyPressed === 27) { //Escape
				event.preventDefault();
				dropDownEvents.hide();
				$(this).closest('.rel-level1').children('div').children('a').focus();
			}
		});

		navi.find(dropDownSelector + ' .submenu-container').attr('aria-expanded', false);
		navi.find(dropDownSelector + ' .submenu-container').attr('aria-role', 'navigation');

        navi.find(".rel-level2").each(function () {
            //if level2 menu have children
            if($(this).parents('#header') > 0) {
                if ($(this).find("ul").length) {
                    $(this).addClass("submenu");
                    $(this).parents('.rel-level1').addClass('wide-nav');
                }
            }


            //if level2 menu should be navigation-image variant
            if ($(this).find('> img').length) {
                $(this).addClass("submenu navigation-image");
            }
        });
    }

    function mobileNavigation(navi) {

        function checkChildren(nav) {
            nav.find(".rel-level1").each(function () {
                if (!$(this).find("ul").length) {
                    $(this).addClass("no-child");
                }
            });
        }

        function bindEvents(nav) {
            nav.find(".rel-level1").on("click", function (e) {
                var navlvl = $(this),
                    menuParent = navlvl.parents('.navigation');

                if (menuParent.hasClass('navigation-mobile')) {
                    if (!$(e.target).is("a")) {
                        if (navlvl.hasClass("active")) {
                            navlvl.find("ul").slideToggle(function () {
                                navlvl.removeClass("active");
                            });
                        } else {
                            navlvl.find("ul").slideToggle(function () {
                                navlvl.addClass("active");
                            });
                        }
                    }
                }
            });

            nav.find(".rel-level1 > a").on("focus", function () {
                $(this).siblings("ul").slideDown();
                $(this).parent().siblings().find("ul").slideUp();
            });

			$('#header .mobile-menu-button').click(function(e) {
                e.preventDefault()
				$('#header .mobile-menu').toggleClass('open');
				$('body').toggleClass('mobile-menu-open');
			});

			$('.mobile-menu .mobile-menu-close').click(function() {
				$('#header .mobile-menu').toggleClass('open');
				$('body').toggleClass('mobile-menu-open');
			});

			$('#header .mobile-search-button').click(function(e) {
                e.preventDefault();
				$('#header .mobile-menu').toggleClass('open');
				$('body').toggleClass('mobile-menu-open');
				$('.mobile-menu .search-box input[id]').focus();
			});

			$('.mobile-menu .search-box input, .mobile-menu .search-box button').keyup(function() {
				var keyPressed = event.keyCode || event.which;
				if (keyPressed === 13) {
					$('.mobile-menu').removeClass('open');
					$('body').removeClass('mobile-menu-open');
				}
			});

			$('.mobile-menu .search-box button').click(function() {
				$('.mobile-menu').removeClass('open');
				$('body').removeClass('mobile-menu-open');
			});
        }

        checkChildren(navi);
        bindEvents(navi);
    }

	function linkListExpandable(linkList) {
		/* linkList.on('click', 'a', function(event) {
			event.preventDefault();
			if (linkList.hasClass('expanded')) {
				if ($('#career-search-heading').text() === $(this).text()) {
					linkList.removeClass('expanded');
					$('#career-search-filter').val('');
				} else {
					$('#career-search-heading').text($(this).text());
					$('#career-search-filter').val($(this).text());
				}
			} else {
				linkList.addClass('expanded');
				$('#career-search-heading').text($(this).text());
				$('#career-search-filter').val($(this).text());
			}
		});

		linkList.on('click', 'h3', function(event) {
			if (linkList.hasClass('expanded')) {
				event.preventDefault();
				linkList.removeClass('expanded');
				$('#career-search-filter').val('');
			}
		}); */
	}

    function toggleIcon(toggle){
        $(toggle).on("click", function(){
            $(this).parents(".navigation").toggleClass("active");
            $(this).toggleClass("active");
        });
    }

    var api = {};

    api.init = function() {
        var navigation = $(".navigation:not(.initialized)");

        navigation.each(function() {
            if ($(this).hasClass("navigation-main")) {
                dropDownNavigation($(this));
                //mobileNavigation($(this));
            } else if ($(this).hasClass("navigation-mobile")) {
                mobileNavigation($(this));
            }

            $(this).addClass("initialized");
        });

        var toggle = $(".mobile-nav:not(.initialized)");
        toggle.each(function(){
            toggleIcon(this);
            $(this).addClass("initialized");
        });

		var linkLists = $('.link-list-expandable');
		linkLists.each(function() {
			var content = $(this).siblings('.link-list-expandable-content');
			if (content.length > 0) {
				content = content.first();
				$(this).append(content);
			}
			linkListExpandable($(this));
		});
    };

    return api;
}(jQuery, document));

XA.register("navigation", XA.component.navigation);