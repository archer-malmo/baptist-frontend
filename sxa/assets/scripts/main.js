if ($('.field-customimage img').length > 0) {
	$('.field-customimage img').click(function() {
		$('#modal-image').attr('src', $(this).attr('src'));
		$('#image-modal').modal('show');
	});
}

document.documentElement.setAttribute('data-useragent', navigator.userAgent);

$('form[data-wffm]').attr('autocomplete', 'off');

if ($('.all-medical-specialties').length > 0) {
	var totalItems = $('.all-medical-specialties ul li').length
	var numPerColumn = totalItems / 3;
	numPerColumn = Math.ceil(numPerColumn);

	var items = $('.all-medical-specialties ul li');
	$('.all-medical-specialties ul').remove();
	var column = $('<ul class="col-sm-4"></ul>');
	items.each(function(index) {
		$(this).appendTo(column);
		indexPlus1 = index + 1;
		if (indexPlus1 % numPerColumn === 0 || index === items.length - 1) {
			$('.all-medical-specialties .component-content nav').append(column);
			column = $('<ul class="col-sm-4"></ul>');
		}
	});

	$('.all-medical-specialties').addClass('list-fix');

	$('.all-medical-specialties li a').each(function() {
		var text = $(this).text();
		$(this).closest('li').attr('data-text-content', text.toLowerCase());
	});

	function clearFilter() {
		$('#no-results-message').hide();
		$('#show-all-button').hide();
		$('.all-medical-specialties li').show();
		$('.all-medical-specialties').removeClass('filtered');
	}

	function filerServiceList() {
		var searchString = $('.tab-search [name="textBoxSearch"]').val();
		searchString = searchString.trim().toLowerCase();
		if (searchString === '') {
			clearFilter();
			return;
		}
		$('.all-medical-specialties li').hide();
		$('.all-medical-specialties li[data-text-content*="' + searchString + '"]').show();
		$('.all-medical-specialties').addClass('filtered');
		if ($('.all-medical-specialties li:visible').length < 1) {
			$('#no-results-message').show();
		} else {
			$('#no-results-message').hide();
		}
		$('#show-all-button').show();
	}

	$('.tab-search .search-box-button').click(function() {
		filerServiceList();
	});

	$('.tab-search [name="textBoxSearch"]').keydown(function() {
		var keyPressed = event.keyCode || event.which;
		if (keyPressed === 13) {
			filerServiceList();
		}
	});

	$('#show-all-button').click(function() {
		clearFilter();
	});
}

$('header .navigation-main li.submenu.rel-level1').each(function() {
	var innerList = $(this).children('ul').first();
	var items = $(innerList).children('li');
	var numPerColumn = items.length / 3;
	numPerColumn = Math.ceil(numPerColumn);

	$(innerList).remove();
	var column = $('<ul class="col-sm-4"></ul>');
	var outerItem = $(this);
	$(outerItem).append('<div class="submenu-container"></div>');
	outerItem = outerItem.children('.submenu-container').first();
	items.each(function(index) {
		$(this).appendTo(column);
		indexPlus1 = index + 1;
		if (indexPlus1 % numPerColumn === 0 || index === items.length - 1) {
			$(outerItem).append(column);
			column = $('<ul class="col-sm-4"></ul>');
		}
	});
});

if($('button[data-recipient]').length > 0  && $('form.contact-with-location-routing').length == 1){

	$('button[data-recipient]').click(function(){
		var rcpt = $(this).data('recipient');
		//console.log(rcpt);
		var selprefix = 'form.contact-with-location-routing select option';
		var sel = selprefix + '[value*="' + rcpt + '"]';


		if($(sel).length == 0 ){
			sel = 'form.contact-with-location-routing select option[value*="helpdesk@bmhcc.org"]';
		}

		var selbox =  $(sel).first().parent();

		//scroll to element
		selbox.focus();
		$([document.documentElement, document.body]).animate({
			scrollTop: selbox.offset().top
		}, 1000);

		var selopt = selbox.find('option');
		selopt.attr('selected', false);
		$(sel).attr('selected', 'selected');

	});
}

if($('form.contact-with-location-routing').length == 1){

	var sel = 'form.contact-with-location-routing';
	var selform = $(sel);

	window.patientComplaintErrorMessage = 'To submit a patient complaint, please select a location.';
	window.subjectFieldID = 'wffm5005b564919b4b2e8c6778b09bffd9d4_Sections_0__Fields_3__Value';
	window.locationFieldID = 'wffm5005b564919b4b2e8c6778b09bffd9d4_Sections_0__Fields_2__Value';
	window.validationBlockSelector = sel + ' .has-error.has-feedback .list-group';
	window.patientComplaintError = false;

	//User must select a location if they select patient complaint
	function wffmPreSubmitHook(event) {
		if ($('#' + window.subjectFieldID).val() === 'Patient Complaint' && $('#' + window.locationFieldID).val() === 'helpdesk@bmhcc.org') {
			if ($(window.validationBlockSelector + ' .patient-complaint-error').length == 0) {
				setTimeout(function() {
					$(window.validationBlockSelector).append('<li class="list-group-item list-group-item-danger patient-complaint-error">' + window.patientComplaintErrorMessage + '</li>');
				}, 100);
			}
			$('#' + window.locationFieldID).closest('.form-group').removeClass('has-success').addClass('has-error has-feedback');
			window.patientComplaintError = true;
			event.preventDefault();
			event.stopPropagation();
			return false;
		}
	}

	selform.submit(function(event){
		var offset = selform.offset().top - 50;
		$([document.documentElement, document.body]).animate({
			scrollTop: offset
		}, 1000);
	});

	//Clear validation error when they select a location
	$('#' + locationFieldID).change(function() {
		if (patientComplaintError && $(this).val() !== 'helpdesk@bmhcc.org') {
			$(validationBlockSelector + ' .patient-complaint-error').remove();
			$('#' + locationFieldID).closest('.form-group').removeClass('has-error has-feedback').addClass('has-success');
		}
	});

}