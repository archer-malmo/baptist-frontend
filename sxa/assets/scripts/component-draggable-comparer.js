XA.component.draggableComparer = function($) {
	var $comparer;
	var maxProducts;
	var showLabels;

	var initSelector = function() {
		$comparer.find('.sibling-image').draggable({
			opacity: 0.7,
			helper: "clone",
			appendTo: 'body',
			start: function(e) {
				var isNotSelected = $(this).parents('.compare-selector').length == 0;
				var maxNumberAchieved = $comparer.find('.compare-selector').find('img.sibling-image').length >= maxProducts;

				if (isNotSelected && maxNumberAchieved) {
					return false;
				}
			}
		});

		$comparer.find('.compare-selector').droppable({
			accept: '.draggable-comparer .sibling-image',
			drop: function(event, ui) {
				if (ui.draggable.parents('.compare-selector').length > 0) {
					return;
				}
				var $holder = $(this).find('.sibling-image-holder').last();
				var productId = ui.draggable.data('id');
				var $clone = $holder.parent().clone();
				$clone.data('id', productId);
				$clone.find('.sibling-image-holder').append(ui.draggable);
				$holder.parent().before($clone);
				$('.sibling-name[data-id="' + productId + '"]').parent().hide();
				$(this).find('.compare-button').show();

				var maxNumberAchieved = $comparer.find('.compare-selector').find('img.sibling-image').length >= maxProducts;
				if (maxNumberAchieved) {
					$(this).find('.sibling-image-holder').last().hide();
				}
			}
		});

		$comparer.find('.siblings').droppable({
			accept: '.draggable-comparer .sibling-image',
			drop: function(event, ui) {
				var productId = ui.draggable.data('id');
				var $holder = $(this).find('.sibling-image-holder[data-id="' + productId + '"]');
				$holder.append(ui.draggable);

				var maxNumberAchieved = $comparer.find('.compare-selector').find('img.sibling-image').length >= maxProducts;
				if (!maxNumberAchieved) {
					$comparer.find('.compare-selector').find('.sibling-image-holder').last().show();
				}

				$('.compare-selector td').each(function() {
					if ($(this).data('id') == ui.draggable.data('id')) {
						$('.sibling-name[data-id="' + ui.draggable.data('id') + '"]').parent().show();
						$(this).remove();
						if ($('.compare-selector td').length == 1) {
							$('.compare-button').hide();
						}
						return;
					}
				});
			}
		});
	};

	var initBack = function() {
		$comparer.find('.compare-back').click(function(e) {
			e.preventDefault();
			$comparer.find('.component-content').css('height', 'auto');
			$comparer.find('.compare-result').fadeTo(400, 0, function() {
				$(this).find('table').remove();
				$(this).hide();
			});
		});
	};

	var initCompare = function() {
		$comparer.find('.product-list-compare').click(function(e) {
			e.preventDefault();
			$comparer.find('.compare-result table').remove();
			var productsToCompare = $comparer.data('sitecore-id');
			var variantItemId = $comparer.data('variantid');
			var linktarget = $comparer.data('linktarget');
			$comparer.find('.compare-selector tr td:not(:last-child) img').each(function() {
				productsToCompare += ',' + $(this).data('sitecore-id');
			});

			$.ajax({
				url: ('/~/zg-feeds/compareitems?ids=' + productsToCompare + '&variantItemId=' + variantItemId).toLowerCase(),
				type: "GET",
				success: function(serverData) {
					if (serverData.length == 0) {
						console.log('Empty data');
						return;
					}
					var $table = $(serverData);
					var $compareResult = $comparer.find('.compare-result');
					$compareResult.find('.compare-back-holder').before($table);
					$compareResult.fadeTo(400, 1);
					setTimeout(function() {
						var compareResultHeight = $table.outerHeight() + 75;
						$compareResult.parent().height(compareResultHeight);
					});
				},
				error: function(data) {
					console.error(data);
				}
			});
		});
	};

	var pub = {};

	pub.init = function() {
		$('.draggable-comparer').each(function() {
			$comparer = $(this);
			maxProducts = parseInt($comparer.data('max'), 10);
			showLabels = $comparer.data('labels') == 'true' || $comparer.data('labels');
			//SXA - after closing the properties dialog all images has visibility='hidden' and initialization fails
			$(".sibling-image").css('visibility', 'visible');

			initSelector();
			initCompare();
			initBack();
		});

	};

	return pub;
}(jQuery);

XA.register("draggable-comparer", XA.component.draggableComparer);