/***** Begin Grunt task runner config *****/
module.exports = function(grunt) {
    var outputPath = 'theme';    
    var path = require('path');

    //The theme you want to work on when you run the default/watch command
	var fs = require('fs');
	var currentTheme;
	if (fs.existsSync('./current-theme.js')) {
		currentTheme = require('./current-theme');
	}

    grunt.initConfig({
        //Read the package.json file in this directory to get the list of Grunt tasks installed by NPM
        pkg: grunt.file.readJSON('package.json'),

        //Configure SASS compilation and minification
        sass: {
            build: {
                files: [{
                    expand: true,
                    cwd: 'assets/styles',
                    src: ['**/*.{scss,sass}'],
                    dest: outputPath + '/styles',
                    ext: '.css'
                }
                ],
                options: {
                    style: 'expanded',
                    unixNewlines: true
                }
            }
        },
        //Configure Autoprefixer to add browser prefixes
        autoprefixer: {
            build: {
                files: [{
                    expand: true,
                    cwd: outputPath + '/styles',
                    src: ['main.css'],
                    dest: outputPath + '/styles',
                    ext: '.css'
                }
                ],
                options: {
                    map: true,
                    browsers: ['last 20 versions']
                }
            },
        },
        //Configure JSHint for JavaScript syntax validation and code quality analysis
        jshint: {
            build: {
                src: ['assets/scripts/*.js'],
                options: {
                    bitwise: true,
                    curly: true,
                    eqeqeq: true,
                    forin: true,
                    freeze: true,
                    funcscope: true,
                    futurehostile: true,
                    globalstrict: true,
                    iterator: true,
                    latedef: true,
                    maxcomplexity: 15,
                    maxdepth: 10,
                    maxerr: 10,
                    maxparams: 10,
                    noarg: true,
                    nocomma: true,
                    nonbsp: true,
                    nonew: true,
                    notypeof: true,
                    shadow: 'inner',
                    singleGroups: true,
                    undef: true,
                    unused: true,
                    lastsemic: true,
                    plusplus: false,
                    browser: true,
                    devel: true,
                    jquery: true,
                    qunit: true
                }
            }
        },
        //Configure Uglify to minify JavaScript code
        uglify: {
            build: {
                files: [{
                    expand: true,
                    cwd: 'assets/scripts',
                    src: ['**/*.js'],
                    dest: outputPath + '/scripts',
                    ext: '.min.js'
                }],
                options: {
                    sourceMap: true
                }
            }
        },
        //Configure Imagemin to compress and optimize image files
        imagemin: {
            build: {
                files: [{
                    expand: true,
                    cwd: 'assets/images',
                    src: ['**/*.{png,jpg,gif,svg}'],
                    dest: outputPath + '/images'
                }]
            }
        },
        //Configure copy task to copy files that aren't processed or moved by another task
        copy: {
            css: {
                files: [
                    {expand: true, flatten: true, src: ['assets/styles/**/*.css'], dest: outputPath + '/styles/', filter: 'isFile'}                    
                ],
            },
            js: {
                files: [
                    {expand: true, flatten: true, src: ['assets/scripts/**/*.js'], dest: outputPath + '/scripts/', filter: 'isFile'}                    
                ],
            },
            fonts: {
                files: [
                    {expand: true, cwd: 'assets/fonts/', src: ['**'], dest: outputPath + '/fonts/'}                    
                ],
            },
			'use-theme-baptist-online': {
				files: [{
                    expand: true,
                    cwd: 'assets/styles/baptist-online',
                    src: ['**/*.sass'],
                    dest: 'assets/styles'
                },
				{
                    expand: true,
                    cwd: 'assets/scripts/baptist-online',
                    src: ['**/*.js'],
                    dest: 'assets/scripts'
                },
				{
                    expand: true,
                    cwd: 'grunt-config/baptist-online',
                    src: ['**/*.js'],
                    dest: ''
                }]
			},
			'use-theme-baptist-cancer-center': {
				files: [{
                    expand: true,
                    cwd: 'assets/styles/baptist-cancer-center',
                    src: ['**/*.sass'],
                    dest: 'assets/styles'
                },
				{
                    expand: true,
                    cwd: 'assets/scripts/baptist-cancer-center',
                    src: ['**/*.js'],
                    dest: 'assets/scripts'
                },
				{
                    expand: true,
                    cwd: 'grunt-config/baptist-cancer-center',
                    src: ['**/*.js'],
                    dest: ''
                }]
			},
			'use-theme-baptist-medical-group': {
                files: [{
                    expand: true,
                    cwd: 'assets/styles/baptist-medical-group',
                    src: ['**/*.sass'],
                    dest: 'assets/styles'
                },
				{
                    expand: true,
                    cwd: 'assets/scripts/baptist-medical-group',
                    src: ['**/*.js'],
                    dest: 'assets/scripts'
                },
				{
                    expand: true,
                    cwd: 'grunt-config/baptist-medical-group',
                    src: ['**/*.js'],
                    dest: ''
                }]
            },
            'use-theme-brain-spine-network': {
                    files: [{
                    expand: true,
                    cwd: 'assets/styles/brain-spine-network',
                    src: ['**/*.sass'],
                    dest: 'assets/styles'
                },
                {
                    expand: true,
                    cwd: 'assets/scripts/brain-spine-network',
                    src: ['**/*.js'],
                    dest: 'assets/scripts'
                },
                {
                    expand: true,
                    cwd: 'grunt-config/brain-spine-network',
                    src: ['**/*.js'],
                    dest: ''
                }]
            },
			'use-theme-baptist-microsites': {
				files: [
				{
                    expand: true,
                    cwd: 'assets/styles/baptist-microsites',
                    src: ['**/*.sass'],
                    dest: 'assets/styles'
                },
				{
                    expand: true,
                    cwd: 'assets/scripts/baptist-microsites',
                    src: ['**/*.js'],
                    dest: 'assets/scripts'
                },
				{
                    expand: true,
                    cwd: 'grunt-config/baptist-microsites',
                    src: ['**/*.js'],
                    dest: ''
                }]
			},
			'copy-theme-baptist-online': {
				files: [{
                    expand: true,
                    cwd: 'theme',
                    src: ['**/*'],
                    dest: 'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline'
                }]
			},
			'copy-theme-baptist-cancer-center': {
				files: [{
                    expand: true,
                    cwd: 'theme',
                    src: ['**/*'],
                    dest: 'export/FileStorage/Baptistcancercenter/United States/-/media/Project/Baptist/Baptist/Baptistcancercenter/United States/Baptistcancercenter'
                }]
			},
			'copy-theme-baptist-medical-group': {
                files: [{
                    expand: true,
                    cwd: 'theme',
                    src: ['**/*'],
                    dest: 'export/FileStorage/Baptistmedicalgroup/United States/-/media/Project/Baptist/Baptist/Baptistmedicalgroup/United States/Baptist Medical Group'
                }]
            },
            'copy-theme-brain-spine-network': {
                files: [{
                    expand: true,
                    cwd: 'theme',
                    src: ['**/*'],
                    dest: 'export/FileStorage/Brainspinenetwork/United States/-/media/Project/Baptist/Baptist/Baptistspinenetwork/United States/BrainSpineNetwork'
                }]
            },
			'copy-theme-baptist-microsites': {
                files: [
					{
						expand: true,
						cwd: 'theme',
						src: ['fonts/**/*'],
						dest: 'export/FileStorage/Microsites/Microsite Demo/-/media/Project/Baptist/Baptist/Microsites/Microsite Demo/Microsite'
					},
					{
						expand: true,
						cwd: 'theme',
						src: ['images/**/*'],
						dest: 'export/FileStorage/Microsites/Microsite Demo/-/media/Project/Baptist/Baptist/Microsites/Microsite Demo/Microsite'
					},
					{
						expand: true,
						cwd: 'theme/scripts',
						src: [
							'component-*.js',
							'current-theme.js',
							'fixheight.js',
							'microsites.js',
						],
						dest: 'export/FileStorage/Microsites/Microsite Demo/-/media/Project/Baptist/Baptist/Microsites/Microsite Demo/Microsite/scripts'
					},
					{
						expand: true,
						cwd: 'theme',
						src: ['styles/microsites.css'],
						dest: 'export/FileStorage/Microsites/Microsite Demo/-/media/Project/Baptist/Baptist/Microsites/Microsite Demo/Microsite'
					},
				]
            }
        },
        concat: {
            'baptist-online': {
                src: [
                    'export/FileStorage/Baptistonline/United States/-/media/Feature/Experience-Accelerator/Bootstrap/Bootstrap/Styles/bootstrap.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Base Themes/Core-Libraries/styles/font-awesomemin.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Base Themes/Core-Libraries/styles/fullcalendar.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Base Themes/Core-Libraries/styles/jquery-ui.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Base Themes/Core-Libraries/styles/mediaelementplayer.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Base Themes/Main Theme/styles/author.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Base Themes/Main Theme/styles/file-type-icons.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Base Themes/Main Theme/styles/reset.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-accordion.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-archive.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-breadcrumb.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-carousel.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-column-splitter.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-container.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-cookies.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-divider.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-draggable-comparer.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-event-list.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-feed.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-field-editor.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-file-list.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-flip.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-forms.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-fullcalendar.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-galleria.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-image.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-language-selector.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-link-list.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-map.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-navigation.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-overlay.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-page-list.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-pagination.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-playlist.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-promo.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-richtext-content.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-scroll-list.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-search.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-social-media-share.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-summary-list.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-summary.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-tabs.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-tag-cloud.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-tag-navigation.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-title.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-toggle.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-twitter.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-video.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/component-zenblog.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/wufoo_bootstrap_theme.css',
                    'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/main.css'
                ],
                dest: 'export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/bol-concat-crit.css'
            }
        },
        cssmin: {
            'baptist-online': {
                files: [{
                    expand: true,
                    cwd: './',
                    src: ['export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/bol-concat-crit.css'],
                    ext: '.min.css'
                }]
            },
            'bol-critical': {
                files: [{
                    expand: true,
                    cwd: './',
                    src: ['export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/bol-critical.css'],
                    ext: '.min.css'
                }]
            }
        },
        criticalcss: {
            'baptist-online': {
                options:{
                    url: "https://www.baptistonline.org/about",
                    width: 1200,
                    height: 900,
                    outputfile: './export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/bol-critical.css',
                    filename:  './export/FileStorage/Baptistonline/United States/-/media/Project/Baptist/Baptist/Baptistonline/United States/Baptistonline/styles/bol-concat-crit.min.css',
                    buffer: 1000*1400,
                    ignoreConsole: false
                }
            }
        },
        /*
        Configure the watch task to monitor all src files for changes, then automatically trigger the appropriate build
        process and refresh the browser
        */
        watch: {
            default: {
                files: ['**/*.{html}'],
                tasks: ['quick-build']
            },
            sass: {
                files: ['assets/styles/**/*.{scss,sass}'],
                tasks: ['sass:build', 'autoprefixer:build']
            },
            css: {
                files: ['assets/styles/**/*.css'],
                tasks: ['copy:css']
            },
            js: {
                files: ['assets/scripts/**/*.js'],
                tasks: ['copy:js' /* 'jshint:build', 'uglify:build' */]
            },
            images: {
                files: ['assets/images/**/*.{png,jpg,gif,svg}'],
                tasks: ['imagemin:build']
            },
            fonts: {
                files: ['assets/fonts/**'],
                tasks: ['copy:fonts']
            },
			'copy-to-theme': {
				files: ['assets/**/*'],
				tasks: ['copy:copy-theme-' + currentTheme]
			},
            options: {
                livereload: true
            }
        },
        //Configure output directory cleanup to force re-build
        clean: {
            build: {
                files: [{
                    expand: true,
                    cwd: outputPath,
                    dot: false,
                    src: [
                        'styles/**/*.css',
                        'styles/**/*.css.map',
                        'styles/**/*/',
                        'scripts/**/*.js',
                        'scripts/**/*.js.map',
                        'scripts/**/*/',
                        'images/**/*.{png,jpg,gif,svg}',
                        'images/**/*/',
                        'fonts/**/*.*',
                        'fonts/**/*/',
                    ]
                }],
                //Uncomment this to test the task without actually deleting anything; run with --verbose to see paths)
                /* options: {
                    'no-write': true
                } */
            },
			theme: {
				files: [{
                    expand: true,
                    cwd: 'assets/styles',
                    dot: false,
                    src: [
                        '_variables.sass',
                    ]
                }],
                //Uncomment this to test the task without actually deleting anything; run with --verbose to see paths)
                /* options: {
                    'no-write': true
                } */
			}
        }
    });

    //Register all task packages with Grunt
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-newer');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-criticalcss');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-concat');

    /*
    Runs all tasks and builds the entire project from scratch to prepare for deployment. Unlike "quick-build" (see
    below), this cleans out the src directories first and always forces all tasks to run.
    */
    grunt.registerTask('full-build', [
        'clean:build',
        'sass:build',
        'autoprefixer:build',
        /* 'jshint:build', */
        /* 'uglify:build', */
        'imagemin:build',
        'copy:css',
        'copy:js',
        'copy:fonts'
    ]);

    /*
    Identical to "full-build", but it uses the "newer" task filter to only run tasks if the src files are actually newer.
    This is different from Grunt's default behavior where all tasks are always executed. If the src files aren't newer
    than their corresponding dest files, or they haven't been modified since the last successful run, that task will be
    skipped. By skipping unecessary tasks, it speeds up Grunt and keeps your workflow more efficient.
    */
    grunt.registerTask('quick-build', [
        'newer:sass:build',
        'newer:autoprefixer:build',
        /* 'newer:jshint:build', */
        /* 'newer:uglify:build', */
        'newer:imagemin:build',
        'newer:copy:css',
        'newer:copy:js',
        'newer:copy:fonts'
    ]);

    //Default task to run if you just run "grunt"
    grunt.registerTask('default', [
		'copy:use-theme-' + currentTheme,
        'quick-build',
        'watch'
    ]);

	//Shortcuts/alias commands for some copy tasks, useful for quickly switching themes
	grunt.registerTask('use-theme-bol', [
		'copy:use-theme-baptist-online'
	]);

	grunt.registerTask('use-theme-bcc', [
		'copy:use-theme-baptist-cancer-center'
	]);

	grunt.registerTask('use-theme-bmg', [
		'copy:use-theme-baptist-medical-group'
	]);

    grunt.registerTask('use-theme-bsn', [
        'copy:use-theme-brain-spine-network'
    ]);

	grunt.registerTask('use-theme-microsites', [
        'copy:use-theme-baptist-microsites'
    ]);

    //critcial css
    grunt.registerTask('critical-baptist-online', [
        'concat:baptist-online',
        'cssmin:baptist-online',
        'criticalcss:baptist-online',
        'cssmin:bol-critical'
    ]);

	/*
	Commands to build a specific theme. These will swap out some of the SASS partial files (like _variables.sass) with
	their theme-specific versions, which emulates a useful feature called "conditional imports" (LESS has this feature,
	but SASS does not). Then it will rebuild the project and copy the output files to the correct theme folder.
	*/
	grunt.registerTask('theme-baptist-online', [
		/*'clean:theme',*/
		'copy:use-theme-baptist-online',
		'full-build',
		'copy:copy-theme-baptist-online',
        /*'critical-baptist-online'*/
	]);

	grunt.registerTask('theme-baptist-cancer-center', [
		/*'clean:theme',*/
		'copy:use-theme-baptist-cancer-center',
		'full-build',
		'copy:copy-theme-baptist-cancer-center',
        /*'critical:baptist-cancer-center'*/
	]);

	grunt.registerTask('theme-baptist-medical-group', [
        /*'clean:theme',*/
        'copy:use-theme-baptist-medical-group',
        'full-build',
        'copy:copy-theme-baptist-medical-group',
        /*'critical:baptist-medical-group'*/
    ]);

    grunt.registerTask('theme-brain-spine-network', [
        /*'clean:theme',*/
        'copy:use-theme-brain-spine-network',
        'full-build',
        'copy:copy-theme-brain-spine-network',
        /*'critical:baptist-medical-group'*/
    ]);

	grunt.registerTask('theme-baptist-microsites', [
        /*'clean:theme',*/
        'copy:use-theme-baptist-microsites',
        'full-build',
        'copy:copy-theme-baptist-microsites',
    ]);

};