#!/bin/bash

#Update OS and all packages
apt-get -y update
apt-get -y dist-upgrade

#Enable automatic package updates (only security fixes by default)
apt-get -y install unattended-upgrades
echo 'APT::Periodic::Update-Package-Lists "1";
APT::Periodic::Download-Upgradeable-Packages "1";
APT::Periodic::AutocleanInterval "30";
APT::Periodic::Unattended-Upgrade "1";' > /etc/apt/apt.conf.d/10periodic

#Install time synchronizing service
apt-get -y install ntp

#Install Apache and PHP
apt-get -y install apache2
apt-get -y install php5

#Install and enable PHP modules
#TODO

#Install and enable Apache modules
a2enmod headers

#Install MySQL (disable prompts for automated install)
#Also set the MySQL root password to "password"
export DEBIAN_FRONTEND='noninteractive'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password password'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password password'
apt-get -y install mysql-server

#Setup the development virtual host in Apache
virtualHostConfig=$(cat <<EOF
<VirtualHost *:80>
	ServerName local.dev

	DocumentRoot /vagrant
	<Directory /vagrant>
		Options +Indexes +FollowSymLinks
		AllowOverride All
        Require all granted
	</Directory>

	#Disable all caching
	<FilesMatch "\.(html|css|js|png|jpg|jpeg|gif|svg)$">
		FileETag None
		Header unset ETag
		Header set Cache-Control "max-age=0, no-cache, no-store, must-revalidate"
		Header set Pragma "no-cache"
		Header set Expires "Wed, 11 Jan 1984 05:00:00 GMT"
	</FilesMatch>

	SetEnv APPLICATION_ENVIRONMENT dev

</VirtualHost>
EOF
)
echo "${virtualHostConfig}" > /etc/apache2/sites-available/dev.conf
a2ensite dev

#Create a database for the application
echo 'create database `dev` character set utf8mb4 collate utf8mb4_bin' | mysql -u root -ppassword

#Enable verbose error reporting in PHP
sed -i.bak 's/^error_reporting = .*$/error_reporting = E_ALL/' /etc/php5/apache2/php.ini
sed -i.bak 's/^display_errors = Off$/display_errors = On/i' /etc/php5/apache2/php.ini

#Install xdebug for PHP debugging
apt-get -y install php5-xdebug

#Install phpMyAdmin for managing the DB (disable prompts for automated install)
debconf-set-selections <<< 'phpmyadmin phpmyadmin/debconfig-install boolean true'
debconf-set-selections <<< 'phpmyadmin/mysql/admin-user string root'
debconf-set-selections <<< 'phpmyadmin/mysql/admin-pass password password'
debconf-set-selections <<< 'phpmyadmin/mysql/app-pass password password'
debconf-set-selections <<< 'phpmyadmin/app-password-confirm password password'
debconf-set-selections <<< 'phpmyadmin/reconfigure-websever multiselect none'
debconf-set-selections <<< 'phpmyadmin/database-type select mysql'
debconf-set-selections <<< 'phpmyadmin/setup-password password password'
apt-get -y install phpmyadmin

#Restart services
echo 'Restarting services...'
service apache2 restart
service mysql restart

#Clear shell & MySQL history created by this script
echo 'Clearing shell & MySQL history created by this script...'
> ~/.bash_history
history -cw
> ~/.mysql_history
